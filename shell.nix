let
  pkgs = import <nixpkgs> {};
in
pkgs.mkShell {
  buildInputs = [
    pkgs.nodejs
    pkgs.leiningen pkgs.jdk11 pkgs.clojure
    (pkgs.clj-kondo.override {
      # As of 8/2020, pkgs.graalvm8 isn't building on Hydra.  To build
      # it yourself takes an hour and 24MB RAM.  graalvm8-ee is built
      # from binaries which you have to manually download and add to
      # the nix store.
      graalvm8 = pkgs.graalvm8-ee;
    })
  ];
}
