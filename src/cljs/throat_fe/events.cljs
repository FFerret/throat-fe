(ns throat-fe.events
  (:require
   [re-frame.core :as re-frame]
   [re-graph.core :as re-graph]
   [throat-fe.db :as db]
   [throat-fe.graphql.queries :as queries]
   [day8.re-frame.tracing :refer-macros [fn-traced]]
   [ajax.core :as ajax]
   ))

(re-frame/reg-event-db
 ::initialize-db
 (fn-traced [_ _]
   db/default-db))

(re-frame/reg-event-db
 ::set-active-panel
 (fn-traced [db [_ active-panel]]
   (assoc db :active-panel active-panel)))

(re-frame/reg-event-db
 :set-posts
 (fn-traced [db [_ posts]]
   (assoc db :posts (get posts :posts))))

(defn error-handler [{:keys [status status-text]}]
  (.log js/console (str "something bad happened: " status " " status-text)))

(re-frame/reg-fx
 :ajax-get
 (fn-traced [[url handler]]
   (ajax/GET url
             {:handler         handler
              :error-handler   error-handler
              :response-format :json
              :keywords?       true})))

(re-frame/reg-event-fx
 :load-posts
 (fn-traced [_ [_ url]]
            {:ajax-get [url #(re-frame/dispatch [:set-posts %])]}))

(re-frame/reg-event-fx
 :load-topbar-subs
 (fn-traced [{:keys [db]} [_ _]]
   {:db (assoc db :topbar-subs [])
    :dispatch [::re-graph/query
               queries/get-subs
               {:count 100 :after ""}
               [::set-topbar-subs]]}))

(re-frame/reg-event-fx
   ::set-topbar-subs
   (fn-traced [{:keys [db]} [_ {:keys [data errors] :as payload}]]
     (let [next-page? (get-in data [:getSubs :pageInfo :hasNextPage])
           end-cursor (get-in data [:getSubs :pageInfo :endCursor])
           subs (map #(:node %) (get-in data [:getSubs :edges]))
           subs-so-far (concat (:topbar-subs db) subs)]
       {:db (assoc db :topbar-subs subs-so-far)
        :dispatch-n (list (when next-page?
                            [::re-graph/query
                             queries/get-subs
                             {:count 100 :after end-cursor}
                             [::set-topbar-subs]]))})))
