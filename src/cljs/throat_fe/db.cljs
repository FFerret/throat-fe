(ns throat-fe.db)

(def default-db
  {:active-panel :all-new
   :sort-key :score
   :loading? false
   :posts []
   :topbar-subs []
   })
