(ns throat-fe.graphql.queries)

(def get-subs "
getSubs ($count: Int, $after: String) {
  getSubs(count: $count, after: $after) {
    pageInfo {
      hasNextPage,
      endCursor
    },
    edges {
      node {
        title,
        name,
        creation
      }
    }
  }
}")

(def get-sub "
getSub ($name: String!, $count: Int, $after: String) {
  getSub(name: $name) {
    posts(count: $count, after: $after) {
      ...PostSummary
    }
  }
}")

(def get-sub-sidebar "
getSubSidebar($name: String!) {
  getSub(name: $name) {
    mods {
      name
    },
    sidebar,
    creation,
    subscribers
  }
}")


(def post-summary-fragment "
PostSummary on PostPage {
  edges {
    node {
      title
      id
      posted
      thumbnail
      link
      score
      commentCount
      author {
        name
      }
      sub {
        name
      }
    }
  }
  pageInfo {
    hasNextPage
    endCursor
  }
}")

(def get-home-posts "
getHomePosts($count: Int, $after: String) {
  getHomePosts(count: $count, after: $after) {
    ...PostSummary
  }
}")

(def get-post-content "
getPostContent($id: ID!) {
  getPost(id: $id) {
    content
  }
}")

(def comment-fragment "
Comment on Comment {
  id
  content
  lastEdit
  author {
    name
  }
  upVotes
  downVotes
  time
}")

(def get-post "
getPost($id: ID!) {
  getPost(id: $id) {
    content
    upVotes
    downVotes
    link
    posted
    edited
    thumbnail
    title
    flair
    author {
      name
    }
  }
}")

(def get-comments-for-post "
getCommentsForPost($id: ID!) {
  getPost(id: $id) {
    comments {
      edges {
        node {
          ...Comment
          children(limit: 10) {
            edges {
              node {
                ...Comment
                children(limit: 5) {
                  edges {
                    node {
                      ...Comment
                      children(limit: 2) {
                        edges {
                          node {
                            ...Comment
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}")
