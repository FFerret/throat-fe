(ns throat-fe.core
  (:require
   [throat-fe.events :as events]
   [reagent.dom :as rdom]
   [re-frame.core :as re-frame]
   [re-graph.core :as re-graph]
   [throat-fe.routes :as routes]
   [throat-fe.views :as views]
   [throat-fe.config :as config]
   ))

(defn dev-setup []
  (when config/debug?
    (println "dev mode")))

(defn ^:dev/after-load mount-root []
  (re-frame/clear-subscription-cache!)
  (let [root-el (.getElementById js/document "app")]
    (rdom/unmount-component-at-node root-el)
    (rdom/render [views/main-panel] root-el)))

(defn init []
  (routes/app-routes)
  (re-frame/dispatch-sync [::events/initialize-db])
  (re-frame/dispatch [::re-graph/init {:ws nil}])
  (re-frame/dispatch [:load-posts "/api/v3/post/all"])
  (re-frame/dispatch [:load-topbar-subs])
  (dev-setup)
  (mount-root))
