(ns throat-fe.subs
  (:require
   [re-frame.core :as re-frame]))

;; Extractors.  No computation, just data access.

(re-frame/reg-sub
 ::active-panel
 (fn [db _]
   (:active-panel db)))

(re-frame/reg-sub
 ::posts
  (fn [db _]
   (:posts db)))

(re-frame/reg-sub
 ::topbar-subs
 (fn [db _]
   (:topbar-subs db)))

;; Computational subscriptions.  Each should depend on an extractor,
;; not the entire db.

(re-frame/reg-sub
 ::topbar-sub-names
 (fn [_]
   (re-frame/subscribe [::topbar-subs]))
 (fn [subs _]
   #_(map #(:name %) subs)
   []))
