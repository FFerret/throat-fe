(ns throat-fe.views
  (:require
   [clojure.string :as str]
   [reagent.core :as reagent]
   [re-frame.core :as re-frame]
   [throat-fe.routes :as routes]
   [throat-fe.subs :as subs]
   [goog.Uri :as uri]
   ))


(defn cs [& names]
  (str/join " " (filter identity names)))

(defn sub-name-link
  "Create one of the links to a sub for the top bar and More menu."
  [name ref]
  ;; TODO url-for
  [:li {:ref ref}
   [:a {:href (str "/s/" name)} name]])

(defn sub-name-links
  "Create a list of links to subs, used for the top bar and More menu."
  [sub-names collect-refs]
  (into [:span]
        (map #(sub-name-link % (fn [ref] (apply collect-refs [% ref])))
             @sub-names)))

(defn sub-bar
  "Create the bar with links to all, new and the subs, including the
  overflow More menu."
  []
  (let [sub-names (re-frame/subscribe [::subs/topbar-sub-names])
        moremenu-open (reagent/atom false)

        ;; Collect the elements in the component and once all are ready,
        ;; decide what to show and what to hide in the More menu.
        sub-bar-elements (reagent/atom {:moremenu nil
                                        :sub-bar-links (hash-map)
                                        :moremenu-links (hash-map)})

        ;; Callback functions to collect elements as they are ready.
        set-moremenu-ref (fn [ref] (swap! sub-bar-elements assoc :moremenu ref))
        set-ref-fn (fn [key]
                     (fn [name ref]
                       (swap! sub-bar-elements assoc-in [key name] ref)))

        ;; Decide if all the elements are collected.
        links-ready? (fn [key sub-bar-elems]
                       (= (count @sub-names) (count (key sub-bar-elems))))
        sub-bar-ready? (fn [sub-bar-elems]
                         (and (:moremenu sub-bar-elems)
                              (links-ready? :sub-bar-links sub-bar-elems)
                              (links-ready? :moremenu-links sub-bar-elems)))

        ;; Detect collision between a link on the sub bar and the More menu.
        too-far-right?
        (fn [bar-link right-limit]
          (let [right-side (+ (.-offsetLeft bar-link) (.-clientWidth bar-link))]
            (> right-side right-limit)))

        hide-the-clutter!
        ;; Set the display property of the links so they appear either
        ;; on the sub bar or in the More menu.  Don't show the More menu
        ;; if it is empty.
        (fn [sub-bar-elems]
          (let [right-limit (.-offsetLeft (:moremenu sub-bar-elems))]
            (loop [names @sub-names
                   past-limit? false]
              (if (empty? names)
                (when-not past-limit?
                  (set! (.-display (.-style (:moremenu sub-bar-elems))) "none"))
                (let [name (first names)
                      bar-link (get-in sub-bar-elems [:sub-bar-links name])
                      menu-link (get-in sub-bar-elems [:moremenu-links name])
                      hit-limit? (or past-limit?
                                     (too-far-right? bar-link right-limit))]
                  (if hit-limit?
                    (set! (-> bar-link .-style .-display) "none")
                    (set! (-> menu-link .-style .-display) "none"))
                  (recur (rest names) hit-limit?))))))

        element-watcher (fn [_ _ _ new]
                          (when (sub-bar-ready? new)
                            (remove-watch sub-bar-elements :element-watcher)
                            (hide-the-clutter! new)))]
    (add-watch sub-bar-elements :element-watcher element-watcher)

    (fn []
      [:div.th-subbar.pure-u-1
       [:ul
        [:li [:a {:href (routes/url-for :all)} "all"]]
        [:li [:a {:href (routes/url-for :all-new)} "new"]]
        [:li "|"]
        [sub-name-links sub-names (set-ref-fn :sub-bar-links)]]
       [:div#subsdropdown {:ref set-moremenu-ref}
        [:div.dropdown-toggle.moremenu {:on-click #(swap! moremenu-open not)}
         "More \u25BC"]
        [:ul#hiddensubs {:style {:display (if @moremenu-open
                                            "inline-flex"
                                            "none")}}
         [sub-name-links sub-names (set-ref-fn :moremenu-links)]]]])))

(defn menu []
  [:div.th-navbar.pure-g
   [:div.cw-brand.pure-u-1.pure-u-md-3-24
    [:div.pure-menu
     [:span.motto "Powered by clojurescript and graphql"]]]])

(defn display-post [{:keys [pid title link user sub userstatus thumbnail posted]}]
  [:div.post
   [:div.misctainer
    [:div.votebutton [:span "vote"]]
    [:div.thcontainer
     [:a (if link
           {:href (str "/s/" sub "/" pid)}
           {:target "_blank"
            :rel "noopener nofollow ugc"
            :href link})
      [:div.thumbnail
       (if (and thumbnail (not= thumbnail "") link)
         [:img {:src (str "https://s3-us-west-2.amazonaws.com/throat-test-all/" thumbnail)}]
         [:span "todo"])]]]
    [:div.pbody
     [:div.post-heading
      [:a.title {:target "_blank" :rel "noopener nofollow ugc"
                 :href link } title]
      " "
      (let [domain (.getDomain ^goog.Uri (uri/parse link))]
        [:a.domain {:href (str "/domain/" domain)} (str "(" domain ")")])]
     [:div.author [:span  (str "posted " posted " by " user " on " sub)]]]]])

(defn display-posts [posts]
  (when-not (empty? posts)
    (into [:div] (map display-post posts))))

(defn main []
  (let [panel (re-frame/subscribe [::subs/active-panel])
        posts (re-frame/subscribe [::subs/posts])]
    [:div.pure-u-1.pure-u-md-18-24
     [display-posts @posts]]))

(defn sortbuttons []
  [:div.pure-button-group  {:role :group}
   [:div.pure-g
    [:a.sbm-post.pure-button.button-xsmall.pure-u-md-7-24 {:href (routes/url-for :all)} "Hot"]
    [:a.sbm-post.pure-button.button-xsmall.pure-u-md-7-24 {:href (routes/url-for :all)} "Top"]
    [:a.sbm-post.pure-button.button-xsmall.pure-u-md-7-24 {:href (routes/url-for :all)} "New"]]])


(defn sidebar []
  [:div.sidebar.pure-u-1.pure-u-md-6-24
   [sortbuttons]])


(defn content []
  [:div.pure-g
   [main]
   [sidebar]])


(defn post-panel []
  [:div
   [sub-bar]
   [menu]
   [content]])

;; main

(defn- panels [panel-name]
  (case panel-name
    :all-panel [post-panel]
    :all-new-panel [post-panel]
    [:div]))

(defn show-panel [panel-name]
  [panels panel-name])

(defn main-panel []
  (let [active-panel (re-frame/subscribe [::subs/active-panel])]
    [show-panel @active-panel]))
